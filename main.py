#!/usr/bin/python3
#
# main.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gi

gi.require_version('Gtk', '3.0')

from experiment.application import Application
from gi.repository import Gtk
from ui.window import Window

import argparse
import logging


# Globals
debug = False
filename = None
gui = False
subjects = None
texts_folder = None
verbosity = 0
with_instance_counter = False
picture = -1


def parse_arguments():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--debug', action='store_true',
                        default=False, dest='debug',
                        help='enables debug and tracing information')
    parser.add_argument('-f', '--file', action='store',
                        default=False, dest='file',
                        help='the csv-formatted data file to load')
    parser.add_argument('-g', '--gui', action='store_true',
                        default=False, dest='gui',
                        help='show an user interface')
    parser.add_argument('-s', '--subjects', action='store',
                        default=False, dest='subjects',
                        help='the csv-formatted subjects file to load')
    parser.add_argument('-t', '--texts-folder', action='store',
                        default=False, dest='texts_folder',
                        help='the folder for the text files')
    parser.add_argument('-v', '--verbosity', action='count',
                        default=False, dest='verbosity',
                        help='verbosity of the debug spill')
    parser.add_argument('-i', '--instances', action='store_true',
                        default=False, dest='with_instance_counter',
                        help='counts the instances')
    parser.add_argument('-p', '--picture', action='store',
                        default=-1, dest='picture',
                        help='target picture')

    args = parser.parse_args()

    # Debug
    global debug
    debug = args.debug

    # Verbosity
    global verbosity
    verbosity = args.verbosity

    if debug:

        # When toggling the debug flag on, the verbosity is always
        # at least 1
        if verbosity == 0:
            verbosity = 1

        if verbosity == 1:
            loglevel = logging.INFO
        else:
            loglevel = logging.DEBUG

        logging.basicConfig(level=loglevel,
                            format='%(asctime)s\t%(levelname)10s\t%(message)s',
                            datefmt='%H:%M:%S')

        logging.info("Enabling debug information")

    # Filename
    global filename
    filename = args.file

    if filename:
        logging.info("Loading data file: " + str(filename))

    # Subjects
    global subjects
    subjects = args.subjects

    if subjects:
        logging.info("Loading subjects file: " + str(subjects))

    # GUI
    global texts_folder
    texts_folder = args.texts_folder

    if texts_folder:
        logging.info("Setting text folder file: " + str(texts_folder))

    # GUI
    global gui
    gui = args.gui

    # Instance counter
    global with_instance_counter
    with_instance_counter = args.with_instance_counter

    # Picture
    global picture
    picture = int(args.picture)

    print('Picture: {}'.format(picture))


def run_gui(app):
    Gtk.init(None)

    window = Window(app)
    window.show()

    Gtk.main()


if __name__ == '__main__':

    # Parse command line arguments
    parse_arguments()

    app = Application(filename, subjects, texts_folder, with_instance_counter,
                      picture)
    app.run()

    if gui:
        run_gui(app)
        

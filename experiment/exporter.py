# exporter.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from .cache import Cache
from .common import Concept, ParsedText
from .utils import Utils

import logging
import os
import os.path


class Exporter(object):

    def __init__(self):
        pass

    @staticmethod
    def _get_file_contents(entries, delimiter='\t'):

        # We assume that all rows have the same number of
        # columns - and shout a warning if not
        n_cols = len(entries[0])
        contents = ''

        for entry in entries:

            row = ''

            # Safety check
            if len(entry) != n_cols:
                logging.warning('Rows don\'t have the same number of columns')
                return ''

            # Build the string row
            for i in range(n_cols):

                if i > 0:
                    row += delimiter

                row += str(entry[i]).strip()

            contents += row
            contents += '\n'

        return contents

    @staticmethod
    def export_entries(entries, filename, delimiter='\t'):

        # Build up the file contents
        csv_contents = Exporter._get_file_contents(entries)

        # Check the output file path
        real_file = filename

        if not os.path.isabs(filename):
            l_file = os.path.join(os.getcwd(), filename)

        # Save things up
        with open(real_file, 'w') as f:
            f.write(csv_contents)
            f.close()

    def export_baseline(self, filename='baseline.csv'):
        """
        Exports the baseline model.

        @param filename: the output file name
        """

        cache = Cache()
        entries = [('CONCEPT', 'STRING')]

        for picture in cache.get_pictures():
            for concept in picture.get_concepts():

                # The baseline model contains only the most used
                # realizations of a given concept. Since realizations
                # are already sorted by usage, the first item is always
                # the most used one.
                realization = concept.get_realizations()[0]

                concept_id = concept.get_id()
                real_id = str(concept_id[0]).strip() + \
                          str(concept_id[1]).strip()

                entry = (real_id, realization.get_name())

                # Fill the list of entries to be exported
                entries.append(entry)

        # And save the file
        Exporter.export_entries(entries, filename)

        logging.info('Exported baseline model to {}'.format(filename))

    def export_dataset(self, filename='dataset.csv', with_counter=False):
        """
        Exports the complete dataset.

        @param filename: the output file name
        """

        cache = Cache()
        if with_counter:
            header = ('EXTRA', 'AGREE', 'CONSC', 'NEURO', 'OPEN', 'CONCEPT', 'STRING', 'INSTANCES')
        else:
            header = ('EXTRA', 'AGREE', 'CONSC', 'NEURO', 'OPEN', 'CONCEPT', 'STRING')

        entries = [header]

        for data in cache.get_dataset():

            #personality_tuple = Utils.personality_average(data[0])
            personality_tuple = (data[0].extraversion,
                                 data[0].agreeableness,
                                 data[0].conscientiousness,
                                 data[0].neuroticism,
                                 data[0].openness)
            realization = data[3]

            # Build the real concept id
            concept_id = data[2].get_id()
            real_id = str(concept_id[0]).strip() + str(concept_id[1]).strip()

            # And the final tuple
            if with_counter:
                entry = personality_tuple + (real_id,
                                             realization.get_name(),
                                             realization.get_instances_counter())
            else:
                entry = personality_tuple + (real_id, realization.get_name())


            entries.append(entry)

        # And save the file
        Exporter.export_entries(entries, filename)

        logging.info('Exported complete dataset to {}'.format(filename))

    def export_counters(self, filename='counters.csv'):
        """
        Export the number of instances of each realization.
        """

        cache = Cache()
        entries = [('STRING', 'INSTANCES')]
        realization_dict = {}

        for data in cache.get_dataset():

            realization = data[3]

            if not realization.get_name() in realization_dict:
                realization_dict[realization.get_name()] = 0

            realization_dict[realization.get_name()] += 1

        for entry in realization_dict.keys():
            entries.append((entry, realization_dict[entry]))

        # And save the file
        Exporter.export_entries(entries, filename)

        logging.info('Exported counters to {}'.format(filename))

    def export_concept_map(self, filename='w2c_map.csv'):
        """
        Export the number of instances of each realization.
        """

        cache = Cache()
        entries = [('CONCEPT', 'STRING', 'INSTANCES')]
        realization_dict = {}
        in_dataset = {}
        counters_dict = {}

        # Fill in the counters and in_dataset dicts
        for data in cache.get_dataset():

            realization = data[3]

            if not realization.get_name().strip() in in_dataset:
                in_dataset[realization.get_name().strip()] = None

            if not realization.get_name().strip() in counters_dict:
                counters_dict[realization.get_name().strip()] = 0

            counters_dict[realization.get_name().strip()] += 1


        for realization in cache.get_realizations():

            name = realization.get_name().strip()

            # ignore realizations not in dataset
            if name not in in_dataset:
                continue

            if name in realization_dict:
                realization_dict[name].append(realization)
            else:
                realization_dict[name] = [realization]


        for name in realization_dict.keys():

            concepts = {}

            for realization in realization_dict[name]:
                for concept in realization.get_concepts():

                    if concept not in concepts:
                        concepts[concept] = None

            for concept in concepts.keys():

                if (len(concept.get_realizations()) <= 1):
                    continue

                concept_id = concept.get_id()
                real_id = str(concept_id[0]).strip() + str(concept_id[1]).strip()

                entries.append((real_id,
                                name,
                                counters_dict[realization.get_name().strip()]))

        # And save the file
        Exporter.export_entries(entries, filename)

        logging.info('Exported concept mapping to {}'.format(filename))

    def export_synsets(self, filename='synsets'):
        """
        Export the synsets after being filtered.
        """

        cache = Cache()
        entries = [('ID', 'TYPE', 'WORDS')]

        # Gather synsets and format its words into a comma-separated list
        for synset in cache.get_synsets():

            words = ''
            first = True

            for word in synset.get_words():

                if first:
                    first = False
                else:
                    words += ','

                words += word.strip()

            entries.append((synset.get_id(), synset.get_type(), words))

        # And save the file
        Exporter.export_entries(entries, filename)

    def export_words_by_class(self, wtype, filename='word_by_type.csv', pic_id=-1):
        """
        Export a dataset with the words of the given class using the
        synsets as unique identifiers.
        """

        cache = Cache()
        parsed_texts = cache.get_parsed_texts()
        entries = [('EXTRA', 'AGREE', 'CONSC', 'NEURO', 'OPEN', 'CONCEPT',
                     'STRING')]

        def get_concept_ids(cache, picture, word, wtype):
            try:
                # Nouns use the manually annotated concepts
                if wtype == ParsedText.NOUN:
                    realization = cache.get_realization_for_picture(
                        picture.get_id(), word)

                    if realization:
                        return [concept for concept in
                                realization.get_concepts()
                                if len(concept.get_realizations()) > 1]
                    else:
                        return None
                else:
                    synsets = cache.get_synsets_for_word(word)
                    return [synset.get_id() for synset in synsets
                            if synset.get_type() == wtype]

            except Exception:
                return None

        for text in parsed_texts:
            words = text.get_words(wtype)
            picture = text.get_picture()

            # Filter by picture
            if pic_id > -1 and pic_id != int(picture.get_id()):
                continue

            personality = text.get_subject().get_personality()
            personality_tuple = (personality.extraversion,
                                 personality.agreeableness,
                                 personality.conscientiousness,
                                 personality.neuroticism,
                                 personality.openness)

            for (word, word_type, counter) in words:
                concepts = get_concept_ids(cache, picture, word, word_type)

                # Ignore invalids
                if not concepts:
                    continue

                for concept in concepts:

                    for index in range(counter):

                        # The concept may be a plain string, or a Concept
                        # instance. If it's a Concept instance, transform
                        # into a string; otherwise, prepend an 's'.
                        if isinstance(concept, Concept):
                            concept_id = concept.get_id()[1]
                        else:
                            concept_id = 's{}'.format(concept)

                        entry = personality_tuple + (concept_id, word)
                        entries.append(entry)

        # And save the file
        Exporter.export_entries(entries, filename)

    def export_concepts(self, filename='concepts'):

        cache = Cache()
        entries = [('PICTURE', 'TYPE', 'WORD')]

        # Gather synsets and format its words into a comma-separated list
        for picture in cache.get_pictures():

            concept_to_words = {}
            parsed_texts = cache.get_parsed_texts(picture.get_id())

            # Fill in concept_to_words dict
            for parsed_text in parsed_texts:
                words = parsed_text.get_words(ParsedText.NOUN)

                for word in words:

                    try:
                        realization = cache.get_realization_for_picture(
                            picture.get_id(), word[0])
                        concepts = realization.get_concepts()

                        for concept in concepts:
                            concept_id = concept.get_id()[1]

                            if concept_id not in concept_to_words:
                                concept_to_words[concept_id] = []

                            # Only append the word if it's unique
                            if word[0] not in concept_to_words[concept_id]:
                                concept_to_words[concept_id].append(word[0])

                    except Exception:
                        continue

            # Add the entries
            for concept in concept_to_words:
                for word in concept_to_words[concept]:
                    entries.append((concept, 'N', word.strip()))

        # And save the file
        Exporter.export_entries(entries, filename)

        logging.info('Exported concepts to {}'.format(filename))

    def export_counter(self, wtype, filename, picture_id=-1):

        cache = Cache()
        parsed_texts = cache.get_parsed_texts()
        counters = {}
        entries = [('CONCEPT', 'COUNTER')]

        def get_concept_ids(cache, picture, word, wtype):
            try:
                # Nouns use the manually annotated concepts
                if wtype == ParsedText.NOUN:
                    realization = cache.get_realization_for_picture(
                        picture.get_id(), word)

                    if realization:
                        return [concept for concept
                                in realization.get_concepts()
                                if len(concept.get_realizations()) > 1]
                    else:
                        return None
                else:
                    synsets = cache.get_synsets_for_word(word)
                    return [synset.get_id() for synset in synsets
                            if synset.get_type() == wtype]

            except Exception:
                return None

        for text in parsed_texts:
            words = text.get_words(wtype)
            picture = text.get_picture()

            # Filter by picture
            if picture_id > -1 and picture_id != int(picture.get_id()):
                continue

            for (word, word_type, counter) in words:
                concepts = get_concept_ids(cache, picture, word, wtype)

                # Ignore invalids
                if not concepts:
                    continue

                for concept in concepts:

                    for index in range(counter):

                        # The concept may be a plain string, or a Concept
                        # instance. If it's a Concept instance, transform
                        # into a string; otherwise, prepend an 's'.
                        if isinstance(concept, Concept):
                            concept_id = concept.get_id()[1]
                        else:
                            concept_id = 's{}'.format(concept)

                        if concept_id not in counters:
                            counters[concept_id] = 0

                        counters[concept_id] += 1

        # Add the entries
        for concept_id in counters.keys():
            entries.append((concept_id, counters[concept_id]))

        def _key(entry):
            if isinstance(entry[1], int):
                return entry[1]
            else:
                return 99999999999

        entries = sorted(entries, key=_key, reverse=True)

        # And save the file
        Exporter.export_entries(entries, filename)

# cache.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from .common import Concept, Picture, Realization, Subject, Text

import logging


class Cache(object):

    # Singleton implementation
    __instance = None

    def __new__(cls):
        if Cache.__instance is None:
            Cache.__instance = object.__new__(cls)

            logging.info('Initializing cache')

            instance = Cache.__instance
            instance._pictures = {}
            instance._concepts = {}
            instance._realizations = {}
            instance._subjects = {}
            instance._texts = {}
            instance._dataset = None
            instance._synsets = {}
            instance._synsets_for_word = {}
            instance._parsed_texts = {}

        return Cache.__instance

    def add_picture(self, pic_id):
        """
        Add a picture to the cache. If there is another picture
        with the given id, nothing happens.

        :param pic_id: the picture id
        """

        if not pic_id in self._pictures:
            logging.debug('Creating picture with id ' + str(pic_id))

            self._pictures[pic_id] = Picture(pic_id)

    def get_picture_by_id(self, pic_id):
        """
        Retrieves the instance of the Picture class relative
        to the picture id.

        :param pic_id: the picture id
        :return: a Picture instance
        :rtype: Picture
        """

        return self._pictures[pic_id]

    def get_pictures(self):
        """
        Retrieves the list of instances to the Picture class.

        :return: a list of Picture instances
        :rtype: list
        """

        return self._pictures.values()

    def add_realization(self, pic_id, name):
        """
        Adds a concept realization relative to the picture id.

        :param pic_id: the picture id this concept comes from
        :param name: the concept realization string
        """

        real_id = (pic_id, name)

        if not real_id in self._realizations:
            logging.debug('Creating concept realization for ' + str(real_id))

            realization = Realization(name)
            realization.set_picture(self._pictures[pic_id])

            self._realizations[real_id] = realization

    def get_realizations(self):
        """
        Retrieves the list of the available realization instances.

        :return: a list of realization instances
        :rtype: list
        """

        return self._realizations.values()

    def get_realization_for_picture(self, pic_id, name):
        """
        Retrieves the instance of Realization class with the
        given name, in the given picture.

        :param pic_id: the picture id
        :param name: the name of the realization
        :return: the realization instance for the given name
        :rtype: Realization
        """

        return self._realizations[(pic_id, name)]

    def remove_realization(self, realization):
        """
        Removes the given realization from the cache.
        """

        real_id = (realization.get_picture().get_id(),
                   realization.get_name())

        if not real_id in self._realizations:
            logging.warning('Cannot remove realization ' + str(real_id))
            return

        # Remove from the cache
        del self._realizations[real_id]

    def add_concept(self, pic_id, concept):
        """
        Adds the concept relative to the picture id.

        :param pic_id: the picture id this concept comes from
        :param concept: the concept id
        """

        real_id = (pic_id, concept)

        if real_id in self._concepts:
            return

        logging.debug('Creating concept for ' + str(real_id))

        concept = Concept(real_id)
        concept.set_picture(self._pictures[pic_id])

        picture = self._pictures[pic_id]
        picture.add_concept(concept)

        self._concepts[real_id] = concept

    def get_concept_for_picture(self, pic_id, concept):
        """
        Retrieves the instance of Concept class with the
        given name, in the given picture.

        :param pic_id: the picture id
        :param name: the concept id
        :return: the concept instance for the given id
        :rtype: Concept
        """

        return self._concepts[(pic_id, concept)]

    def get_concepts(self):
        """
        Retrieves all the available Concept instances.

        :return: a Concept list
        :rtype: list
        """

        return self._concepts.values()

    def add_subject(self, id, source, gender, personality):
        """
        Adds the subject to the cache.

        :param id: id of the subject
        :param source: source of the subject's data
        :param gender: the gender of the subject
        :param age: the age of the subject
        :param personality: personality of the subject
        """

        if id in self._subjects:
            logging.warning('Subject {} to the list of subjects'\
                            .format(str(id)))
            return

        logging.debug('Creating subject {} '.format(id))

        self._subjects[id] = Subject(id, source, gender, personality)

    def get_subject_by_id(self, id):
        """
        Retrieves the instance of Subject with the given
        identifier.

        :param id: the id of the subject
        :return: the subject with the given id
        :rtype: Subject
        """

        return self._subjects[id]

    def get_subjects(self):
        """
        Retrieves the list of Subject instances available.

        :return: a list of Subject instances
        :rtype: list
        """

        return self._subjects.values()

    def get_texts(self):
        """
        Retrieves all available texts and captions.

        :return: a list of Text instances
        :rtype: list
        """

        return self._texts.values()

    def add_text(self, pic_id, subject_id, data):
        """
        Adds a text to the cache.

        @param pic_id: the picture id
        @param subject_id: the id of the subject that wrote the text
        @param data: the content
        """

        # XXX: support for captions is still incomplete
        real_id = (pic_id, subject_id, Text.ORIGINAL)

        if real_id in self._texts:
            logging.warning('Cannot add the same text {} twice'\
                            .format(str(real_id)))
            return

        logging.debug('Creating text for {}'.format(str(real_id)))

        self._texts[real_id] = Text(Text.ORIGINAL,
                                    self._pictures[pic_id],
                                    self._subjects[subject_id],
                                    data)

        self._subjects[subject_id].add_text(self._texts[real_id])

    def set_dataset(self, dataset):
        """
        Sets the built dataset
         @dataset: the dataset
        """

        logging.debug('Setting dataset...')

        self._dataset = dataset

    def get_dataset(self):
        """
        Retrieves the main dataset

        :return: a list of tuples
        :rtype: list
        """

        return self._dataset

    def add_synset(self, synset):
        """
        Adds a synset to the cache.

        @param synset: the Synset instance
        """

        id = str(synset.get_id())

        if id in self._synsets:
            logging.warning('Synset {} already added'.format(id))
            return

        self._synsets[id] = synset

        # Add the words to the reverse map (word → list of synsets)
        for word in synset.get_words():
            if word not in self._synsets_for_word:
                self._synsets_for_word[word] = [synset]
            else:
                self._synsets_for_word[word].append(synset)

    def get_synset(self, synset_id):
        """
        Retrieves the synset with the given.

        :return: the synset with the given id
        :rtype: Synset
        """

        return self._synsets[str(synset_id)]

    def get_synsets(self):
        """
        Retrieves all the synsets.

        :return: a list of Synset instances
        :rtype: list
        """

        return self._synsets.values()

    def get_synsets_for_word(self, word):
        """
        Retrieves all the synsets of a given word.

        :return: a list of Synset instances
        :rtype: list
        """

        return self._synsets_for_word[word]

    def clear_synsets(self):
        """
        Clear all synsets from the cache.
        """
        self._synsets.clear()
        self._synsets_for_word.clear()

    def add_parsed_text(self, parsed_text):
        """
        Adds a parsed text to the cache.

        :param parsed_text: a ParsedText instance
        """

        pic_id = parsed_text.get_picture().get_id()

        if pic_id not in self._parsed_texts:
            self._parsed_texts[pic_id] = []

        self._parsed_texts[pic_id].append(parsed_text)

    def get_parsed_texts(self, pic_id=-1):
        """
        Retrieves all parsed texts available for the given picture,
        or for all pictures if -1 is passed.

        :param pic_id: the number of the Picture, or -1 for all
        :return: a list of all parsed texts
        :rtype: list
        """

        if int(pic_id) >= 0:
            return list(self._parsed_texts[pic_id])

        tmp = []
        for texts in self._parsed_texts.values():
            tmp.extend(texts)
        return tmp

# filter.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from .cache import Cache
from .synsets import Synset

import logging


class Filter(object):

    def __init__(self):
        self.cache = Cache()

    def filter_concepts(self):
        """
        Removes all the valid concepts.

        :return: a filtered list
        :rtype: list
        """

        # Start by filtering out realizations with few occurrences
        logging.info('Filtering concepts...')

        self._filter_by_occurrences()

        # Remove concepts with only one realization
        self._filter_by_realizations()

        logging.info('Done')

    def _filter_by_realizations(self):
        """
        Filters out all the concepts with only one realization.
        """

        valid_concepts = []
        concepts = self.cache.get_concepts()

        logging.info('Filtering concepts with one realization...')

        # Each picture has a list of concepts, and each
        # concept has a list of realizations. We only
        # want the concepts with more than one realization.

        for concept in concepts:

            # Skip concepts with more than one realization
            if len(concept.get_realizations()) <= 1:
                continue

            valid_concepts.append(concept)

        # Now, remove from the dataset
        dataset = self.cache.get_dataset()

        # Create a new copy of the dataset without the
        # invalid concepts
        dataset = [data for data in dataset if data[2] in valid_concepts]

        # And then set the dataset again
        self.cache.set_dataset(dataset)


    def _filter_by_occurrences(self, min_ocurrences=5):

        dataset = self.cache.get_dataset()
        realizations = {}
        removed_realizations = []

        logging.info('Filtering realizations with few occurrences...')

        for data in dataset:

            realization = data[3]

            # Skip concepts with more than one realization
            if not realization in realizations:
                realizations[realization] = 0

            realizations[realization] += 1

        for realization in realizations.keys():

            if realizations[realization] > min_ocurrences:
                continue

            removed_realizations.append(realization)

        # Create a new copy of the dataset without the
        # invalid concepts
        dataset = [data for data in dataset if data[3] not in removed_realizations]

        # And then set the dataset again
        self.cache.set_dataset(dataset)

    def filter_synsets(self):
        """
        Remove synsets whose words are not in the corpora, or with only
        one word.
        """

        cache = Cache()
        synsets = cache.get_synsets()
        duplicated_synsets = []
        final_synsets = []

        # The unique words in the corpus after it is filtered
        corpus_words = []

        for parsed_text in cache.get_parsed_texts():

            # Use {} to maintain uniqueness
            words = { entry[0] for entry in parsed_text.get_words() if not entry[0] in corpus_words }

            # Cast to list(), otherwise we won't really append the elements but
            # the entire set()
            corpus_words.extend(list(words))

        # Run over the synsets and filter out the ones with 1 or 0 words
        # in the corpus
        for synset in synsets:

            words_in_corpus = 0
            synset_words = synset.get_words()

            # Number of words shared between the corpus and this synset
            shared_words = corpus_words & synset_words

            # We only want synsets with 2+ words from the corpora
            if len(shared_words) < 2:
                continue

            # Copy the current synset and add only the words in the corpus
            new_synset = Synset(synset.get_id(), synset.get_type())

            for word in shared_words:
                new_synset.add_word(word)

            duplicated_synsets.append(new_synset)

        # The synsets can possibly be redundant, and potentially there are
        # synsets with the exact same words. Now we deduplicate them
        for i in range(len(duplicated_synsets)):

            synset_a = duplicated_synsets[i]
            words_a = set(synset_a.get_words())

            has_duplicated = False

            for j in range(i + 1, len(duplicated_synsets)):

                synset_b = duplicated_synsets[j]
                words_b = set(synset_b.get_words())

                if len(words_a.intersection(words_b)) == len(words_a):
                    print('Synset {} has duplicated {}'.format(synset_a.get_id(), synset_b.get_id()))
                    has_duplicated = True
                    break

            # Only add to the final synsets when it doesn't have a duplicated.
            if not has_duplicated:
                final_synsets.append(synset_a)

        # Remove the current synsets entirely
        cache.clear_synsets()

        for synset in final_synsets:
            cache.add_synset(synset)

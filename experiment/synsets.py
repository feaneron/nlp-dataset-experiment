# synsets.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from enum import Enum

import logging


class Synset(object):
    def __init__(self, id, synset_type):
        self._synset_type = synset_type
        self._opposite = None
        self._words = {}
        self._id = id

    def get_id(self):
        """
        Retrieves the unique identifier of this group.

        :returns: unique identifier of the synset
        :rtype: int
        """
        return self._id

    def get_type(self):
        """
        Retrieves the synset type.

        :returns: synset type
        :rtype: str
        """
        return self._synset_type

    def add_word(self, word):
        """
        Adds the word to the synset.

        :param word: the word (str) to add
        """
        self._words[word] = None

    def has_word(self, word):
        """
        Retrieves whether the word is in the synset.

        :param word: the word (str) to check for
        :returns: True if the word is in the synset
        :rtype: bool
        """
        return word in self._words

    def get_words(self):
        """
        Retrieves all the words of this synset.

        :returns: list of words
        :rtype: list
        """
        return self._words.keys()

    def set_opposite(self, opposite):
        """
        Sets the opposite synset (with antonyms).

        :param opposite: the opposite synset
        """
        self._opposite = opposite

    def get_opposite(self):
        """
        Retrieves the antonyms of this synset.

        :returns: the antonym synset
        :rtype: Synset or None
        """
        return self._opposite

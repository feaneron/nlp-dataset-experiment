# loader.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from .cache import Cache
from .common import Personality, Text, ParsedText
from .synsets import Synset

import csv
import glob
import logging
import re
import os
import os.path
import string


class DataLoader(object):

    def __init__(self, csvfile, subjects_file, texts_folder):
        logging.info('Loading data loader')

        self._file = csvfile
        logging.info('Setting data file to ' + str(csvfile))

        self._subjects_file = subjects_file
        logging.info('Setting subjects file to ' + str(subjects_file))

        self._texts_folder = texts_folder
        logging.info('Setting texts folder to ' + str(texts_folder))

    def load_subjects(self):
        """
        Load the subjects in a CSV file. The file must have a header
        specifying the index of:

            - Id
            - Source of the personality traits
            - Gender
            - The personality traits (from the Big Five model)
        """

        cache = Cache()

        logging.info('Parsing subjects...')

        with open(self._subjects_file, 'r') as f:
            contents = f.read()
            split_content = contents.split('\n')

            header = True

            for row in split_content:
                fields = row.split(',')

                # Ignore comments
                if row.startswith('#'):
                    continue

                # Ignore empty rows
                if row == '':
                    continue

                try:
                    id = int(fields[0])
                    source = fields[1]
                    gender = fields[3]

                    personality = Personality(float(fields[8]),
                                              float(fields[9]),
                                              float(fields[10]),
                                              float(fields[11]),
                                              float(fields[12]))

                    cache.add_subject(id, source, gender, personality)

                except Exception as e:
                    logging.critical(e)
                    continue

        logging.info('Done')


    def load_data(self, delimiter='\t'):
        """
        Loads and parses the data. It must be in a CSV-like format.
        @param delimiter: the delimiter
        """
        cache = Cache()

        logging.info('Parsing data...')

        with open(self._file, 'r') as f:
            contents = f.read()
            split_content = contents.split('\n')

            for row in split_content:

                # Ignore comments
                if row.startswith('#'):
                    continue

                # Ignore empty rows
                if row == '':
                    continue

                row_data = row.split('\t')

                # The first field is the picture id, followed by the
                # concept realization and the concepts themselves
                picture_id = row_data[0]
                realizations = row_data[1].split(',')
                concept_ids = row_data[2].split(',')

                # First field is the picture id
                cache.add_picture(picture_id)

                picture = cache.get_picture_by_id(picture_id)

                for realization_name in realizations:

                    # Second field is the concept realization
                    cache.add_realization(picture_id, realization_name.strip())

                    realization = cache.get_realization_for_picture(picture_id,
                                                                    realization_name)
                    # Add this realization to the picture
                    picture.add_realization(realization)

                    # Third field is a comma-separated list of concepts
                    for concept_id in concept_ids:

                        cache.add_concept(picture_id, concept_id)

                        # Link the concept with this picture and realization
                        c = cache.get_concept_for_picture(picture_id,
                                                          concept_id)

                        c.add_realization(realization)

                        # And also the realization with the concept
                        realization.add_concept(c)

        logging.info('Parsing finished')

    def load_texts(self):
        """
        Reads and loads all the texts and captions from the texts folder.
        """

        logging.info('Parsing text files...')

        regex = re.compile('(text|caption)([0-9]+)-(original|parsed)\.txt')
        path = os.path.join(os.getcwd(), self._texts_folder, '*.txt')

        for file_path in glob.glob(path):

            file_name = os.path.basename(file_path)

            # Check if the file name matches our expectations
            match = regex.match(file_name)

            if match == None:
                continue

            ctype = match.group(1)
            pic_id = match.group(2)
            parsed = match.group(3)

            # XXX: for now, we only load non-parsed texts
            if parsed == 'original' and ctype != Text.CAPTION:
                self._load_original_text(file_path, pic_id)
            elif parsed == 'parsed' and ctype != Text.CAPTION:
                self._load_parsed_text(file_path, pic_id)


        logging.info('Done')

    def _load_original_text(self, path, pic_id):
        """
        Loads the text content of a file.

        @param path: the file path
        """

        logging.info('  Loading \'{}\''.format(os.path.basename (path)))

        cache = Cache()
        regex = re.compile('([0-9]+),(.*)')

        with open(path, 'r') as f:
            contents = f.read()
            rows = contents.split('\n')

            # Parse row by row
            for row in rows:
                match = regex.match(row)

                # Sanity check (we're always sure this is correct)
                if match == None:
                    continue

                subject_id = int(match.group(1))
                text_data = match.group(2)

                # HACK: to remove trailing zeroes, do a str/int/str
                # roundtrip
                real_pic_id = str(int(pic_id))

                # Add to the cache
                cache.add_text(real_pic_id, subject_id, text_data)

    def _load_parsed_text(self, path, pic_id):
        """
        Loads the text content of a file.

        @param path: the file path
        """

        logging.info('  Loading parsed \'{}\''.format(os.path.basename (path)))

        cache = Cache()
        picture = cache.get_picture_by_id(str(int(pic_id)))
        subject_regex = re.compile('<TEXT ID=([0-9]+)>')
        words_regex = re.compile('^(.*)\[(.*)\] ((<.*> )*)(([A-Z0-9\/]+ )*)(@.*)')

        def parse_subject(f, regex):
            # It al starts with the ID of the subject
            line = f.readline()

            if not line:
                return -1

            match = regex.match(line)

            # Sanity check (we're always sure this is correct)
            if match == None:
                return -1

            return int(match.group(1))

        def parse_words(f, text, regex):
            """Parse the words after inside the <text></text> node"""

            line = f.readline().replace('\n', '')

            # The only stop point is when reaching </text>
            while line != '</TEXT>':

                # Match the words_regex expression
                match = regex.match(line)

                if not match:
                    line = f.readline().replace('\n', '')
                    continue

                root = match.group(2)
                classes = match.group(5).strip().split(' ')

                # Add to the ParsedText object
                parsed_text.add_word(root, classes[0])

                line = f.readline().replace('\n', '')

        with open(path, 'r') as f:

            while True:
                # Start by the subject id
                subject_id = parse_subject(f, subject_regex)

                # EOF
                if subject_id == -1:
                    break

                subject = cache.get_subject_by_id(subject_id)
                parsed_text = ParsedText(Text.PARSED, picture, subject, None)

                # Add all the words, with their classes, to the ParsedText
                # instance
                parse_words(f, parsed_text, words_regex)

                cache.add_parsed_text(parsed_text)

    def build_dataset(self):
        """
        Builds the main dataset and stores in the cache
        """

        cache = Cache()
        entries = []

        # Utility function to gather all text realizations
        def parse_text_realizations(text):
            pic_id = text.get_picture().get_id()
            realizations = []
            translator = str.maketrans('', '', string.punctuation)

            split_text = text.get_text().strip().lower().split()

            for word in split_text:

                # Remove punctuation
                word = word.translate(translator).strip()

                try:

                    # Retrieves the realization, if available
                    realization = cache.get_realization_for_picture(pic_id, word)

                    realizations.append(realization)

                except Exception as e:

                    # No realization found for this word, just continue
                    continue

            return realizations

        for text in cache.get_texts():

            subject = text.get_subject()
            personality = subject.get_personality()
            realizations = parse_text_realizations(text)

            # Iterate over every realization found in the given
            # text. It doesn't matter if the same realization is
            # repeated.
            for realization in realizations:

                for concept in realization.get_concepts():

                    # Increase the instance counter
                    realization.push_instance()

                    # Now we generate the entry for the final list
                    entry = (personality,
                             concept.get_picture(),
                             concept,
                             realization)

                    entries.append(entry)

        cache.set_dataset(entries)

    def load_synsets(self):
        """
        Load the TEP dictionary synsets.
        """

        logging.info('Loading synsets from TEP dictionary...')

        cache = Cache()
        regex = re.compile('([0-9]+). \[([a-zA-Z]+)\] \{([\w,. ]+)\} (<[0-9]>){0,1}')

        def parse_synset_from_row(row, regex):
            """Parse a synset from row"""

            match = regex.match(row)

            if match == None:
                return None

            id = int(match.group(1))
            stype = match.group(2)
            words = match.group(3).split(', ')
            agroup = match.group(4)

            # Antonyms may or may not exist
            if agroup != None:
                antonym_id = int(agroup.replace('<', '').replace('>', ''))
            else:
                antonym_id = None

            # Translate the stype into ParsedText classes
            if stype == 'Substantivo':
                synset_type = ParsedText.NOUN
            elif stype == 'Adjetivo':
                synset_type = ParsedText.ADJECTIVE
            elif stype == 'Advérbio':
                synset_type = ParsedText.ADVERB
            else:
                synset_type = ParsedText.VERB

            # Create the synset
            synset = Synset(id, synset_type)

            for word in words:
                synset.add_word(word)

            return synset

        with open('data/tep/base_tep2.txt', 'r') as f:
            contents = f.read()
            rows = contents.split('\n')

            for row in rows:
                synset = parse_synset_from_row(row, regex)

                if not synset:
                    continue

                cache.add_synset(synset)

        logging.info('Done')

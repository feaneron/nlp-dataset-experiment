# utils.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from .cache import Cache


class Utils(object):

    _personality_averages = None

    @staticmethod
    def _calc_average_personalities():

        # Look for the cached value
        if Utils._personality_averages != None:
            return Utils._personality_averages

        cache = Cache()

        counter = 0
        extra = 0.0
        agree = 0.0
        consc = 0.0
        neuro = 0.0
        openn = 0.0

        for subject in cache.get_subjects():

            # Don't consider subjects without texts
            if len(subject.get_texts()) == 0:
                continue

            personality = subject.get_personality()

            extra += personality.extraversion
            agree += personality.agreeableness
            consc += personality.conscientiousness
            neuro += personality.neuroticism
            openn += personality.openness

            counter += 1

        Utils._personality_averages = (extra / counter,
                                       agree / counter,
                                       consc / counter,
                                       neuro / counter,
                                       openn / counter)

    @staticmethod
    def personality_average(personality):

        Utils._calc_average_personalities()

        averages = Utils._personality_averages

        def equal_or_higher(v1, v2):
            if v1 > v2:
                return 'yes'
            else:
                return 'no'


        result = (equal_or_higher(personality.extraversion, averages[0]),
                  equal_or_higher(personality.agreeableness, averages[1]),
                  equal_or_higher(personality.conscientiousness, averages[2]),
                  equal_or_higher(personality.neuroticism, averages[3]),
                  equal_or_higher(personality.openness, averages[4]))

        return result

    @staticmethod
    def str_len_double(value):
        return '{:.4f}'.format(value)

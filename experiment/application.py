# application.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from .cache import Cache
from .common import ParsedText
from .exporter import Exporter
from .filter import Filter
from .loader import DataLoader

import logging


class Application(object):

    def __init__(self, csvfile, subjects_file, texts_folder, with_instance_counter, picture):
        logging.info('Initializing application...')

        # Default file
        if not csvfile:
            csvfile = 'data/data.csv'

        # Default subjects file
        if not subjects_file:
            subjects_file = 'data/subjects.csv'

        # Default texts folder
        if not texts_folder:
            texts_folder = 'data/texts'

        self.loader = DataLoader(csvfile, subjects_file, texts_folder)
        self.filter = Filter()
        self.exporter = Exporter()

        self._with_instance_counter = with_instance_counter
        self._picture = picture

    def run(self):
        logging.info('Running application')

        # Load all the available data
        self.loader.load_subjects()
        self.loader.load_data()
        self.loader.load_texts()
        self.loader.load_synsets()

        # Build the full dataset
        self.loader.build_dataset()

        # Filter the noise out
        self.filter.filter_concepts()
        self.filter.filter_synsets()

        # Export the models
        if self._with_instance_counter:
            self.exporter.export_dataset('counted_datased.csv',
                                         self._with_instance_counter)

        self.exporter.export_dataset()
        self.exporter.export_baseline()
        self.exporter.export_counters()
        self.exporter.export_concept_map()
        self.exporter.export_synsets()
        self.exporter.export_words_by_class(ParsedText.VERB, 'verbs.csv', self._picture)
        self.exporter.export_words_by_class(ParsedText.ADJECTIVE, 'adjs.csv', self._picture)
        self.exporter.export_words_by_class(ParsedText.NOUN, 'nouns.csv', self._picture)
        self.exporter.export_concepts()

        for i in range(10):

            picture_id = i + 1

            self.exporter.export_counter(
                ParsedText.ADJECTIVE,
                'adjectives_counter{}'.format(picture_id),
                picture_id)
            self.exporter.export_counter(
                ParsedText.VERB,
                'verbs_counter{}'.format(picture_id),
                picture_id)
            self.exporter.export_counter(
                ParsedText.NOUN,
                'nouns_counter{}'.format(picture_id),
                picture_id)

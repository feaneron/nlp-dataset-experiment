# statistics.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from .cache import Cache
from .exporter import Exporter
from .utils import Utils

import logging


class Statistics(object):

    def __init__(self):
        pass

    @staticmethod
    def precision(true_positives, false_positives):
        return true_positives / (true_positives + false_positives)

    @staticmethod
    def recall(true_positives, total_positives):
        if total_positives == 0:
            return 0
        else:
            return true_positives / total_positives

    @staticmethod
    def f_measure(precision, recall):
        if precision + recall == 0:
            return 0
        else:
            return 2 * (precision * recall / (precision + recall))

    def _export_results(self, results, filename='test-baseline'):

        average = [0, 0, 0, 0, 0]
        n_entries = len(results)

        # Headers
        entries = [('TP Rate', 'FP Rate', 'Preci.', 'Recall', 'F-Meas.', 'Class')]

        for res in results.keys():
            total = results[res]['total']
            fp_rate = results[res]['false_positives'] / total
            tp_rate = results[res]['hits'] / total
            precision = results[res]['precision']
            recall = results[res]['recall']
            f_measure = results[res]['f_measure']

            entries.append((Utils.str_len_double(tp_rate),
                            Utils.str_len_double(fp_rate),
                            Utils.str_len_double(precision),
                            Utils.str_len_double(recall),
                            Utils.str_len_double(f_measure),
                            res))

            # Update the averages
            average[0] += tp_rate
            average[1] += fp_rate
            average[2] += precision
            average[3] += recall
            average[4] += f_measure

        # Calculate the final average values
        for i in range(5):
            average[i] /= n_entries

        # And append it as the last entry
        entries.append((Utils.str_len_double(average[0]),
                        Utils.str_len_double(average[1]),
                        Utils.str_len_double(average[2]),
                        Utils.str_len_double(average[3]),
                        Utils.str_len_double(average[4]),
                        "AVERAGES"))

        Exporter.export_entries(entries, filename)

    def test_baseline(self):
        """
        Tests the baseline model, and returns the statistics about
        it.

        :return: a tuple with precision, recall and f-measure
        :rtype: tuple
        """

        logging.info('Testing baseline model...')

        cache = Cache()
        dataset = cache.get_dataset()
        realization_data = {}

        # Counters
        total = len(dataset)

        # Build the baseline model - it's quite easy
        baseline = self._get_baseline()

        for data in dataset:

            concept = data[2]
            realization = data[3]
            realization_name = realization.get_name()

            # Add to the dict if not there already
            if not realization_name in realization_data:
                realization_data[realization_name] = {}
                realization_data[realization_name]['hits'] = 0
                realization_data[realization_name]['total'] = 0
                realization_data[realization_name]['false_positives'] = 0

            # Increase total counter, regardless of the correctness
            realization_data[realization_name]['total'] += 1

            if realization_name == baseline[concept].get_name():
                realization_data[realization_name]['hits'] += 1

        for realization in realization_data.keys():

            # The baseline always hits the majority class, and
            # misses the others. Thus, false_positives = misses,
            # false_negatives = 0 and misses = 0
            total = realization_data[realization]['total']
            true_positives = realization_data[realization]['hits']
            false_positives = total - true_positives
            false_negatives = 0
            true_negatives = 0

            precision = Statistics.precision(true_positives, false_positives)

            # Baseline doesn't have false negatives
            recall = Statistics.recall(true_positives, true_positives)
            f_measure = Statistics.f_measure(precision, recall)

            realization_data[realization]['precision'] = precision
            realization_data[realization]['recall'] = recall
            realization_data[realization]['f_measure'] = f_measure

        #print(realization_data)
        self._export_results(realization_data)

        logging.info('Done')

    def _get_baseline(self):

        cache = Cache()
        entries = {}

        for picture in cache.get_pictures():
            for concept in picture.get_concepts():

                # Since realizations are already sorted by usage, the
                # first item is always the most used one.
                realization = concept.get_realizations()[0]

                entries[concept] = realization

        return entries

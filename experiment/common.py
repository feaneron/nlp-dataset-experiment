# common.py
#
# Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import logging


class Picture(object):

    def __init__(self, pic_id):
        self._id = pic_id
        self._realizations = []
        self._concepts = []

    def get_id(self):
        """
        Retrieves the picture id

        :return: the id of the picture
        :rtype: int
        """

        return self._id

    def add_realization(self, realization):
        """
        Adds the realization to the picture.

        :param realization: a Realization instance
        """

        if realization in self._realizations:
            logging.warning('Cannot add already added realization "{}" to picture "{}"'\
                            .format(realization.get_name(), self._id))
            return

        self._realizations.append(realization)

    def get_realizations(self):
        """
        Retrieve the list of realizations of this image.

        :return: a list of Realization instances
        :rtype: Realization
        """

        return self._realizations

    def add_concept(self, concept):
        """
        Adds the concept to the picture.

        :param concept: a Concept instance
        """

        if concept in self._concepts:
            logging.warning('Cannot add already added concept "{}" to picture "{}"'\
                            .format(concept.get_id(), self._id))
            return

        self._concepts.append(concept)

    def get_concepts(self):
        """
        Retrieve the list of concepts of this image.

        :return: a list of Concept instances
        :rtype: Concept
        """

        return self._concepts


# ---------------------------------------------------------------------


class Realization(object):

    def __init__(self, name):
        self._name = name
        self._picture = None
        self._concepts = {}
        self._instances = 0

    def get_name(self):
        """
        Retrieves the name of the concept realization.

        :return: the human-readable name of this realization
        :rtype: string
        """

        return self._name

    def get_picture(self):
        """
        Retrieves the picture this realization is related to.

        :return: a Picture instance
        :rtype: Picture
        """

        return self._picture

    def set_picture(self, picture):
        """
        Sets the picture this realization is related to.

        :param picture: a Picture instance
        """

        self._picture = picture

    def get_concepts(self):
        """
        Retrieves the concepts related to this realization.

        :return: a list of Concept instances
        :rtype: list
        """

        return self._concepts.values()

    def add_concept(self, concept):
        """
        Adds the concept to the list of concepts related to this
        realization.

        @param concept: a Concept instance
        """

        if concept.get_id() in self._concepts:
            logging.warning('Cannot add the same concept {} twice'\
                            .format(str(concept.get_id())))
            return

        self._concepts[concept.get_id()] = concept

    def push_instance(self):
        """
        Increases the instance counter.
        """

        self._instances += 1

    def pop_instance(self):
        """
        Decreases the instance counter
        """

        self._instances -= 1

    def get_instances_counter(self):
        """
        Retrieves the number of instances that the realization has.

        :return: the number of instances
        :rtype: int
        """

        return self._instances


# ---------------------------------------------------------------------


class Concept(object):

    def __init__(self, concept_id):
        self._id = concept_id
        self._picture = None
        self._realizations = []

    def get_id(self):
        """
        Retrieves the concept id

        :return: the id of the concept
        :rtype: int
        """

        return self._id

    def get_realizations(self):
        """
        Retrieves the list of available realizations of this concept.

        :return: a list of Realization instances
        :rtype: list
        """

        return self._realizations

    def add_realization(self, realization):
        """
        Adds a realization of this concept

        :param realization: a Realization instance
        """

        if realization in self._realizations:
            logging.warning('Cannot add already added realization "{}" to concept "{}"'\
                            .format(realization.get_name(), self._id))
            return

        logging.debug('Adding realization \'{}\' to concept {}'\
                      .format(realization.get_name(), self._id[1]))

        self._realizations.append(realization)

    def get_picture(self):
        """
        Retrieves the picture this concept is related to.

        :return: a Picture instance
        :rtype: Picture
        """

        return self._picture

    def set_picture(self, picture):
        """
        Sets the picture this concept is related to.

        :param picture: a Picture instance
        """

        self._picture = picture


# ---------------------------------------------------------------------


class Personality(object):

    def __init__(self, extraversion, agreeableness, conscientiousness, neuroticism, openness):
        self.extraversion = extraversion
        self.agreeableness = agreeableness
        self.conscientiousness = conscientiousness
        self.neuroticism = neuroticism
        self.openness = openness


# ---------------------------------------------------------------------


class Subject(object):

    def __init__(self, id, source, gender, personality):
        self._id = id;
        self._source = source
        self._gender = gender
        self._personality = personality
        self._texts = []

    def get_id(self):
        """
        Retrirves the id of the subject.

        :return: the id of the subject
        :rtype: int
        """

        return self._id

    def get_source(self):
        """
        Retrieves the source of this subject's data. Can be "facebook"
        and "presential".

        :return: the source of the subject data
        :rtype: string
        """

        return self._source

    def get_gender(self):
        """
        Retrieves the gender of the subject. Can be "male" or "female".

        :return: the gender of the subject
        :rtype: string
        """

        return self._gender

    def get_personality(self):
        """
        Retrieves the personality of the subject.

        :return: Retrieves the personality of the subject
        :rtype: Personality
        """

        return self._personality

    def get_texts(self):
        """
        Retrieves the texts this subject wrote.

        :return: a list of Text instances
        :rtype: list
        """

        return self._texts

    def add_text(self, text):
        """
        Add a text to the list of texts this subject wrote.

        @param text: a Text instance
        """

        if text in self._texts:
            logging.warning('Cannot add duplicated texts to subject {}'\
                            .format(self._id))
            return

        logging.debug('Adding text to subject {}'.format(self._id))

        self._texts.append(text)


# ---------------------------------------------------------------------


class Text(object):

    ORIGINAL = 'original'
    CAPTION  = 'caption'
    PARSED = 'parsed'

    def __init__(self, type, picture, subject, text):
        self._subject = subject
        self._picture = picture
        self._text = text
        self._type = type

    def get_text(self):
        """
        Retrieves the content of this text.

        :return: the text
        :rtype: string
        """

        return self._text

    def get_subject(self):
        """
        Retrieves the subject that wrote this text.

        :return: a Subject instance
        :rtype: Subject
        """

        return self._subject

    def get_picture(self):
        """
        Retrieves the picture this text comes from.

        :return: the picture this text describes
        :rtype: Picture
        """

        return self._picture


class ParsedText(Text):

    ADJECTIVE='ADJ'
    ADVERB='ADV'
    NOUN='N'
    VERB='V'

    def __init__(self, type, picture, subject, text):
        super().__init__(type, picture, subject, text)

        # Maps (word, type) → counter
        self._words = {}

    def add_word(self, word, word_type):
        """
        Adds a word to the wordlist of this parsed text.

        :param word: a word (str)
        :param word_type: the word type (noun, verb, adjective, adverb)
        """

        word_id = (word, word_type)

        if word_id not in self._words:
            self._words[word_id] = 0

        self._words[word_id] += 1

    def get_words(self, word_type=None):
        """
        Retrieves all the words from this text.

        :param word_type: the word type
        :return: a tuple of word, type and number of instances
        :rtype: tuple(str,str,int)
        """
        if word_type == None:
            return [(id[0], id[1], counter) for id, counter in self._words.items()]
        else:
            return [(id[0], id[1], counter) for id, counter in self._words.items() \
                    if id[1] == word_type]

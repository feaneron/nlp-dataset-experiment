
Concepts
---------

 * Concept: a concept of the scene inside the picture. Each concept is unique
    inside the picture (but not outside). A concept must have at least one
    realization associated. For example: concept _h1_ in picture 2.

 * Realization: a word representing a concept. Each realization is internal to
    the picture. If the same word is present in texts of different pictures -
    such as "criança" in pictures 1 and 2 - they are treated as different
    realizations. Inside a picture, it's a N→N mapping; a realization can
    represent multiple concepts, and a concept can have multiple realizations.

 * Subject: someone who did the experiment. Has a personality modeled after the
    Big Five personality theory, gender and a unique identifier. No names are
    stored. For example: male subject _1320_ with personality {1, 2, 3, 4, 5}

 * Picture: a picture in the experiment. Each subject inputs one text and one
    caption. Each picture has then N texts, where N is the number of subjects.



Structure
---------

 * main.py
     Handle command line arguments, and starts the app.

 * application.py
     Organize and coordinate the other modules. Instanciates other modules,
     proxies command line params to them, and execute them in a logical order.

 * cache.py
     Singleton class, stores all data representations in memory. It also creates
     the instances of the concepts (the Loader class only pass raw data, and
     the Cache class instances the classes defined in common.py)

 * common.py
     Models the concepts described above in Python.

 * exporter.py
     Exports the dataset, baseline and everything else. If something saves data
     somewhere, it should be written here.

 * filter.py
     Removes noise and unecessary data from the dataset.

 * loader.py
     Loads all the dataset in memory. Only pass raw data to Cache, and delegates
     creating instances to the cache.

from gi.repository import Gtk, Gio

import logging


class Window(Gtk.Window):

    def __init__(self, app):
        logging.info('Initializing UI...')

        self._app = app

        # Hook up
        Gtk.Window.__init__(self, default_width=800, default_height=600)

        try:
            self._builder = Gtk.Builder.new_from_file('ui/window.ui')
        except Exception as err:
            logging.critical('Error loading main window')
            self.destroy()
            return

        # Load UI components
        self.add(self._builder.get_object('main_box'))

        # Header bar
        titlebar = Gtk.HeaderBar(title='Digite um texto para ser reescrito',
                                 show_close_button=True)
        titlebar.show()

        self.set_titlebar(titlebar)

        self.connect('delete-event', Gtk.main_quit)
